import pandas as pd

unsorted=pd.read_csv("sample.csv")

print("The existing data base is\n",unsorted)

ch=int(input("\nChoose the row you want to be sorted\n1-Name\n2-Height\n3-Weight\n4-GPA\nEnter your choice= "))

if(ch==1):
	sorted1=unsorted.sort_values(by=['Name'])
	sorted1.to_csv("sorted.csv")
	print("Above Database has been Sorted and saved")
elif(ch==2):
	sorted1=unsorted.sort_values(by=['Height'])
	sorted1.to_csv("sorted.csv")
	print("Above Database has been Sorted and saved")
elif(ch==3):
	sorted1=unsorted.sort_values(by=['Weight'])
	sorted1.to_csv("sorted.csv")
	print("Above Database has been Sorted and saved")
elif(ch==4):
	sorted1=unsorted.sort_values(by=['GPA'])
	sorted1.to_csv("sorted.csv")
	print("Above Database has been Sorted and saved")

else:
	print("Invalid Input!\nTry Again")
